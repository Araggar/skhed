import requests
import json


class Strawpoll:

	def __init__(self, url="https://www.strawpoll.me/api/v2/polls", p_id=None):
		self.url = url
		self.id = p_id
		self.session = requests.Session()
		self.session.headers.update({'User-Agent':'StrawpollBot', "Content-Type": "application/json"})

	def create_poll(self, title, options, multi=True, captcha=False, dupcheck="normal"):
		query = {
			'title' : title,
			'options' : options, # List of options
			'multi' : multi,
			'captcha' : captcha,
			'dupcheck' : dupcheck
		}
		resp = self.session.post(self.url, json=query).json()
		print(resp)
		return resp['id']

	def poll_status(self, p_id=None):
		resp = self.session.get(self.url+"/"+str(p_id or self.id)).json()
		return zip(resp['options'], resp['votes'])

	def update_id(self, p_id):
		self.id = p_id

	def poll_link(self):
		return "https://www.strawpoll.me/{}".format(self.id)


if __name__ == "__main__":
	Strawpoll().create_poll("Dn", ['Yes', 'No'])
