import requests
import re
from time import sleep
from sys import argv, exit

import strawpoll

class Skhed:

	def __init__(self, name, token, options, headers={'User-Agent':'SkhedBot'}):
		self.name = name
		self.chat_id = ""
		self.__token = token
		self.base_url = "https://api.telegram.org/bot{}".format(self.__token)+"/{method}"
		self.session = requests.Session()
		self.session_headers(headers)
		self.update_count = len(self.get_updates().json()['result'])
		self.strawpoll = strawpoll.Strawpoll()
		self.new_strawpoll(options)

	def session_headers(self, header_dict):
		self.session.headers.update(header_dict)

	# Request handler
	def requester(self, method, post=()):
		built = method+"?"
		for k, v in post:
			built += "{}={}&".format(k, v)
		return self.session.get(self.base_url.format(method=built))

	# GET Requests
	def get_me(self):
		return self.requester('getMe')

	def get_updates(self):
		return self.requester('getUpdates')

	# POST Requests
	def send_message(self, text):
		self.requester("sendMessage", (("chat_id",self.chat_id),("text", text)))

	# Client Methods
	def c_update(self, link="",*_):
		if link:
			self.strawpoll.update_id(link)
			text = "Link updated"
			self.send_message(text)
			self.c_strawpoll()
		else:
			text = "Invalid link"
			self.send_message(text)

	def c_strawpoll(self, *_):
		self.send_message(self.strawpoll.poll_link())

	def c_help(self, *_):
		text = "Commands: help, strawpoll, update, status"
		self.send_message(text)

	def c_status(self, *_):
		text = ""
		for o, v in self.strawpoll.poll_status():
			text += "{} - {}\n".format(o, v)
		self.send_message(text)

	def c_new(self, *args):
		self.strawpoll.update_id(
			self.strawpoll.create_poll(
				args[0][0],
				args[0][1:]))
		self.c_strawpoll()


	# Server methods
	def new_strawpoll(self, options, *_):
		self.strawpoll.update_id(self.strawpoll.create_poll(
			self.name,
			options,
			"true",
			"false",
			"normal"))
		self.c_strawpoll()

	def get_last_message(self, results):
		last_update = len(results['result']) - 1
		text = results["result"][last_update]["message"]["text"]
		return text

	def update_chat_id(self, results):
		last_update = len(results['result']) - 1
		self.chat_id = results["result"][last_update]["message"]["chat"]["id"]

	# Main loop
	def main(self):
		while True:
			sleep(0.5)
			updates = self.get_updates().json()
			if len(updates['result']) == self.update_count:
				continue

			self.update_count = len(updates['result']) # Only does last request, change to do every req on the queue?
			command = self.get_last_message(updates)
			self.update_chat_id(updates)
			try:
				action = re.findall('\/([\w\W]+)@skhed_bot([\w\W]*)', command)
				method = getattr(self, "c_{}".format(action[0][0]), self.c_help)
				method(action[0][1].split("|"))
			except Exception as err:
				print(action)




if __name__ == "__main__":
        try:
            argv[3]
        except Exception:
            print("Use: python3 {} name token option1 [option2 ...]".format(argv[0]))
            exit(1)
        skhed = Skhed(argv[1], argv[2], argv[3:])
        skhed.main()
	





